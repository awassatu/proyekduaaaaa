# If the user is not

# If docker is installed continue if not install it
if ! which docker > /dev/null; then
  echo -e 'Docker not found, installing it now.'
  apt install docker.io -y
fi

echo 'Docker is installled continuing...' 

echo "Please input your ngrok authtoken (dashboard.ngrok.com): "
read authtoken

echo "Installing ngrok"

curl -s https://ngrok-agent.s3.amazonaws.com/ngrok.asc | sudo tee /etc/apt/trusted.gpg.d/ngrok.asc >/dev/null && echo "deb https://ngrok-agent.s3.amazonaws.com buster main" | sudo tee /etc/apt/sources.list.d/ngrok.list && sudo apt update && sudo apt install ngrok -y

ngrok authtoken $authtoken > /dev/null &
ngrok http 3000 > /dev/null &

echo "Done!"

echo "Running docker script."

docker run -d \
  --name=webtop \
  --security-opt seccomp=unconfined \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ=Europe/London \
  -e SUBFOLDER=/ \
  -e KEYBOARD=en-us-qwerty \
  -e TITLE=Webtop \
  -p 3000:3000 \
  -v /path/to/data:/config \
  -v /var/run/docker.sock:/var/run/docker.sock \
  --restart unless-stopped \
  lscr.io/linuxserver/webtop:ubuntu-kde

echo "Please look at the output below and find the \"Tunnel Adress\" to access your RDP! (Username: abc, password: abc)"
curl http://localhost:4040/api/tunnels
